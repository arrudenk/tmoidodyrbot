import datetime

from telegram import ParseMode
from telegram.ext import CommandHandler

from src.DataHandler import newweek, dropdb, setweek, clearstatus, getcurrentweekslaves, updatestatus, getstatus, setupcurrentweekslaves
from src.DataStorage import LocalStorage

from src.Updater import updater

dispatcher = updater.dispatcher
j_q = updater.job_queue
localdb = LocalStorage()

def setupnewweek():
    newweek()

def setnextweek(update, context):
    setupnewweek()

def callback_minute(context):
    today = datetime.datetime.today()
    weekday = today.weekday()
    if weekday == 0 and localdb.weekday == 6:
        setupnewweek()
        localdb.weekday = 0
    if not weekday == localdb.weekday:
        localdb.weekday = weekday
    if today.time().strftime("%H:%M") == '20:30':
        status("", context)
    if today.time().strftime("%H:%M") == '23:59':
        context.bot.send_message(chat_id=localdb.chat_id, text="Псс ребята давайте не шуметь")


def reset_db(update, context):
    if "@" + update.message.from_user.username == localdb.admin:
        dropdb()
        setweek(0)
        clearstatus()
        setupcurrentweekslaves()
        context.bot.send_message(chat_id=update.effective_chat.id, text="Data is reset. \n Week is <b>0</b>.",
                                 parse_mode=ParseMode.HTML)


def start_scheduler():
    return updater.job_queue.run_repeating(callback_minute, interval=60, first=60)


def start(update, context):
    print(update.message.from_user.username)
    context.bot.send_message(chat_id=update.effective_chat.id, text="Started")
    localdb.started = True
    localdb.chat_id = update.effective_chat.id
    dispatcher.add_handler(CommandHandler('job', start_scheduler(), pass_job_queue=True))


def done(update, context):
    updatestatus("@" + update.message.from_user.username)
    context.bot.send_message(chat_id=localdb.chat_id,
                             text="CONGRATULATIONS @" + update.message.from_user.username)


def status(update, context):
    statuslog = "Hello!"
    slaves = getcurrentweekslaves()
    for slave in slaves:
        if getstatus(slave):
            isdone = ": <b>Сделано + </b>"
        else:
            isdone = ": <b>Не Сделано - </b>"
        job = LocalStorage.locations[slaves.index(slave)]
        statuslog = statuslog + "\n" + slave + " -> " + job + isdone
    context.bot.send_message(chat_id=localdb.chat_id, text=statuslog, parse_mode=ParseMode.HTML)


start_handler = CommandHandler('start', start)
status_handler = CommandHandler('info', status)
done_handler = CommandHandler('done', done)
resetdb_handler = CommandHandler('resetdb', reset_db)
nextweek_handler = CommandHandler('new_week', setnextweek)

dispatcher.add_handler(start_handler)
dispatcher.add_handler(status_handler)
dispatcher.add_handler(done_handler)
dispatcher.add_handler(resetdb_handler)
dispatcher.add_handler(nextweek_handler)

updater.start_polling()
updater.idle()
