from telegram.ext import Updater
import os

with open(os.path.relpath(os.path.dirname(os.path.realpath(__file__)) + '/data/token.txt')) as file:
    TOKEN = file.readline().strip()
updater = Updater(token=TOKEN, use_context=True)
