import sqlite3 as lite

# sqlite3 week.db
# .tables
# sqlite> .mode column
# sqlite> .heareds on
con = lite.connect('../week.db', check_same_thread=False)

# TODO: remove s in the end
slaves = ["@Darksub", "@Maximalistt", "@emptiness13", "@jefffffffffffffffffffff"]


def dropdb():
    with con:
        cur = con.cursor()
        cur.execute("DROP TABLE IF EXISTS week")
        cur.execute("DROP TABLE IF EXISTS isdone")
        cur.execute("DROP TABLE IF EXISTS currentslaves")


def get_table_creation_execute_string(start, fill, count):
    i = 0

    while i < count - 1:
        start += fill + str(i) + " TEXT,"
        i += 1
    start += fill + str(i) + " TEXT)"
    return start


def get_table_insertion_execute_string(start, fill, end, count):
    i = 0

    while i < count - 1:
        start += fill
        i += 1
    start += end
    return start


def setupcurrentweekslaves():
    week = getweek()
    with con:
        cur = con.cursor()
        cur.execute("DROP TABLE IF EXISTS slaves")
        cur.execute(get_table_creation_execute_string("CREATE TABLE slaves(", "name", len(slaves)))
        i = 0
        newslaves = slaves.copy()
        while i < week % len(newslaves):
            newslaves.append(newslaves.pop(0))
            i += 1

        cur.execute(get_table_insertion_execute_string("INSERT INTO slaves VALUES(", "?,", "?)", len(slaves)),newslaves)


def getcurrentweekslaves():
    with con:
        cur = con.cursor()
        cur.execute("SELECT * FROM slaves")
        one = cur.fetchone()
        if one is None:
            return slaves
        else:
            return one


def newweek():
    week = getweek() + 1
    setweek(week)
    clearstatus()
    setupcurrentweekslaves()


def getweek():
    with con:
        cur = con.cursor()
        # get the count of tables with the name
        cur.execute(''' SELECT count(name) FROM sqlite_master WHERE type='table' AND name='week' ''')
        # if the count is 1, then table exists
        if cur.fetchone()[0] == 1:
            cur.execute("SELECT * FROM week")
            return cur.fetchone()[0]
        return 0


def setweek(week):
    with con:
        cur = con.cursor()
        cur.execute("DROP TABLE IF EXISTS week")
        cur.execute("CREATE TABLE week(number INT)")

        cur.execute("INSERT INTO week VALUES(" + str(week) + ")")


def clearstatus():
    with con:
        cur = con.cursor()
        cur.execute("DROP TABLE IF EXISTS isdone")
        cur.execute("CREATE TABLE isdone(id TEXT)")


def getstatusarray():
    status = []
    with con:
        cur = con.cursor()
        cur.execute("SELECT * FROM isdone")
        while True:
            one = cur.fetchone()
            if one is None:
                break
            else:
                status.append(one[0])
    return status


def getstatus(tid):
    if tid in getstatusarray():
        return True
    else:
        return False


def updatestatus(tid):
    with con:
        cur = con.cursor()

        if getstatus(tid):
            return
        else:
            cur.execute("INSERT INTO isdone VALUES(?)", [tid])
            print("Status were updated " + tid)

